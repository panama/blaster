import numpy as np
import scipy as sp
from joblib import Parallel, delayed

import cmath
from scipy.optimize import minimize_scalar, minimize

import matplotlib.pyplot as plt

class InnerProductResidual():
    '''
        Inner product function between parametric atom and residual
        $$
            \\eta: t -> \\calF_k^\\star (\\vobs - \\calF\\mu) 
        $$
        where 
        $$ 
            \\mu = \\sum_{\\ell=1}{\\#spikes_1} \\coef_1(\\ell) \\delta_{t}
            + \\sum_{\\ell=1}{\\#spikes_2} \\coef_2(\\ell) \\delta_{t}
        $$
    '''
    
    def __init__(self, opObs, bsh, lbd, nnegative=False):
        '''
        Inputs
        ------
            - T1: couple
                A couple of floats for interval 1. e.g. T1= (0, 1.)
            - T2: couple
                A couple of floats for interval 2. e.g. T2= (0, 1.)  
            - bfa1: np.array
                 a complex numpy array of size k
            - bfa2: np.array
                 a complex numpy array of size k
            - bsh: np.array
                a complex vector of size k
            - lbd: float
                lambda parameter
        '''
        self.opObs = opObs
        self.T1 = opObs.T1
        self.T2 = opObs.T2
        self.bfa1 = opObs.bfa1
        self.bfa2 = opObs.bfa2
        self.k = opObs.bfa1.shape[0]
        self._bsh = bsh
        self.fs = opObs.fs
        self.lbd = lbd

        self.freqs = np.arange(0, self.k)

        self.isNnegative = nnegative

        # if self.k % 2 == 1:
        #     self.freqs = np.arange(- int(self.k / 2), int(self.k / 2)+1)
        # else:
        #     self.freqs = np.arange(- int(self.k / 2), int(self.k / 2))

    def evaluate(self, t, iset):
        '''
            evaluate eta in (t,iset)

        Inputs
        ------
            - t: float
                A time interval belonging either to T_1 or T_2
            - iset: int
                an integer indicating whether t belong to set_1 or set_2
                a=1 for set_1 and a=2 for set_2
        '''

        assert(type(iset) == int)
        assert(iset == 1 or iset == 2)

        if iset == 1:
            return self._evaluate_set1(t)
        elif iset == 2:
            return self._evaluate_set2(t)


    def f_par(self, t, iset):
        if self.isNnegative:
            return - self.evaluate(t, iset)
        else:
            return - np.abs(self.evaluate(t, iset))


    def maximize_one_channel(self, T, iset):
        grid = np.linspace(T[0], T[1], 10001)

        fgrid = \
            Parallel(n_jobs=3)(
                delayed(self.f_par)(t, iset) for t in grid)

        fgrid = np.array(fgrid)
        imax = np.argmax(fgrid)
        res = minimize(self.f_par, [grid[imax]], args=(iset), bounds=[T], 
                method='L-BFGS-B')
        return res


    def maximize_par(self):
        '''
            Done using scipy.optimize for now
            edit: parallel version

        Inputs
        ------

        Outputs
        ------
            - res: 3-tuple (x, fun, set)
            where 
                + x is the maximize
                + fun is the maximum of the function
                + set is an integer indicating whether blabla

        '''

        # Maximizer on a grid
        T = [self.T1, self.T2]
        res1, res2 = \
            Parallel(n_jobs=2)(
                delayed(self.maximize_one_channel)(
                    T[i], i+1) for i in range(2))

        # return one of the two max
        if res1.fun < res2.fun:
            #print("max in T1 at " + str(res1.x))
            return (res1.x[0], - res1.fun, 1)
        else:
            #print("max in T2 at " + str(res2.x))
            return (res2.x[0], - res2.fun, 2)

    def maximize(self):
        '''
            Done using scipy.optimize for now

        Inputs
        ------

        Outputs
        ------
            - res: 3-tuple (x, fun, set)
            where 
                + x is the maximize
                + fun is the maximum of the function
                + set is an integer indicating whether blabla

        '''
        
        # Maximizer on a grid
        if 0:
            return self.maximize_par()
        else:
            grid1 = np.linspace(self.T1[0], self.T1[1], 10001)
            grid2 = np.linspace(self.T2[0], self.T2[1], 10001)

            if self.isNnegative:
                f1 = lambda t: - self._evaluate_set1(t)        
                f2 = lambda t: - self._evaluate_set2(t)
            else:
                f1 = lambda t: - np.abs(self._evaluate_set1(t))        
                f2 = lambda t: - np.abs(self._evaluate_set2(t))

            fgrid1 = np.array([-f1(t) for t in grid1])
            fgrid2 = np.array([-f2(t) for t in grid2])

            imax1 = np.argmax(fgrid1)
            imax2 = np.argmax(fgrid2)

            #print([fgrid1[imax1], fgrid2[imax2]])

            # max in set 1
            res1 = minimize(f1, [grid1[imax1]], bounds=[self.T1], method='L-BFGS-B')

            # max in set 2
            res2 = minimize(f2, [grid2[imax2]], bounds=[self.T2], method='L-BFGS-B')

            # return one of the two max
            if res1.fun < res2.fun:
                #print("max in T1 at " + str(res1.x))
                return (res1.x[0], - res1.fun, 1) 
            else:
                #print("max in T2 at " + str(res2.x))
                return (res2.x[0], - res2.fun, 2)


    def _evaluate_set1(self, t):

        return np.real( \
            np.conj(self.opObs.signed_atome_T1(t)) @ self._bsh \
            ) / self.lbd


    def _evaluate_set2(self, t):

        return np.real( \
            np.conj(self.opObs.signed_atome_T2(t)) @ self._bsh \
            ) / self.lbd


    def plot(self, channel, dic_plot={}):
        """
        - dic_plot: dictionary
            contains additional plots stuff
        """

        n1 = len(coeff1)
        n2 = len(coeff2)

        fig, arrax = plt.subplots(2,2, figsize=(16,9), sharex=True, sharey=True)

        ranget1 = np.linspace(self.T1[0], self.T1[1], 1001)
        out1 = 0 * ranget1

        ranget2 = np.linspace(self.T2[0], self.T2[1], 1001)
        out2 = 0 * ranget2

        for i in range(ranget1.shape[0]):
            out1[i] = self._evaluate_set1( ranget1[i] )
            out2[i] = self._evaluate_set2( ranget2[i] )


        xmin = min(min(ranget1), min(ranget2))
        xmax = max(max(ranget1), max(ranget2))

        ymin = min(min(out1), min(out2))
        ymax = max(np.max(np.abs(out1)), np.max(np.abs(out2)))


        arrax[0,0].plot(ranget1, out1)
        arrax[0,0].set_title("Inner product on T1")
        arrax[0,0].set_xlim([xmin, xmax])
        arrax[0,0].set_ylim([ymin, ymax])

        arrax[0,1].plot(ranget2, out2)
        arrax[0,1].set_title("Inner product on T2")
        arrax[0,0].set_ylim([-ymax, ymax])

        if n1 > 0:
            arrax[1,0].stem(np.array(channel.support_1), channel.coeff_1, use_line_collection=True)
            arrax[1,0].set_title("estimated filter 1")
            arrax[1,0].set_xlim([xmin, xmax])

        if n2 > 0:
            arrax[1,1].stem(np.array(channel.support_2), channel.coeff_2, use_line_collection=True)
            arrax[1,1].set_title("estimated filter 2")
            arrax[1,0].set_xlim([xmin, xmax])

        if "maximizer" in dic_plot.keys():
            entry = dic_plot["maximizer"]
            t = entry[0]
            iset = entry[1]

            if iset == 1:
                f = self._evaluate_set1(t)
                arrax[0, 0].plot(np.array([t]), np.array([f]), 'rx', markersize=15)
            elif iset == 2:
                f = self._evaluate_set2(t)
                arrax[0, 1].plot(np.array([t]), np.array([f]), 'rx', markersize=15)
            else:
                raise Exception()

        if "suptitle" in dic_plot.keys():
            fig.suptitle(dic_plot["suptitle"], fontsize=16)
        plt.show()
