import numpy as np
import matplotlib.pyplot as plt

from scipy.linalg import toeplitz, block_diag
from scipy.optimize import minimize, LinearConstraint

from src.dsp_utils import DFT_matrix, same_lenght_pad_with_zeros, lag_finder, pick_echoes
from src.channel import Channel
from src.lasso import Complex_nn_lasso

# from mulan.algorithm import mulan, InitializationType
# from mulan.measurement_tools import get_spectral_coefficients_no_time_processing
# from mulan.baseline_methods import CR

def conv_toepliz(h,m,n):
    padding = np.zeros(h.shape[0] - 1, h.dtype)
    first_col = np.r_[h, padding]
    first_row = np.r_[h[0], padding]
    H = toeplitz(first_col, first_row)
    return H[:m,:n]

def sota(x1,x2,h1,h2,lam):
    h1_coeff, h2_coeff = [], []
    return h1_coeff, h2_coeff

def _print_optimization_results(res):
    print('-- ended after ' + str(res.nit) + ' iterations')
    print('-- obj fun values: ', res.fun)
    print('-- success: ', res.success)
    print(res.message)

def plot_lasso_style(y, A, x, is_A_real=False):
    fig, arrax = plt.subplots(1,3)
    arrax[0].imshow(y[:,None])
    arrax[0].set_xticks([])
    # arrax[0].set_xlegend('Dim (%d, %d)'%(yobs[:,None].shape))
    if is_A_real:
        arrax[1].imshow(A)
    else:
        arrax[1].imshow(np.vstack([np.real(A), np.imag(A)]))
    arrax[2].imshow(x[:,None])
    arrax[2].set_xticks([])

    plt.show()

class Sota_algos():
    '''
    Collection of State of the Art BSI script
    '''
    def __init__(self,algorithm,L,fs,lam,max_iter,do_plot=False):
        self.algorithm = algorithm
        self.max_iter = max_iter
        self.L = L # lenght of the desired filter
        self.fs = fs
        self.fun = np.infty
        self.it = 0
        self.dual_gap = 0
        self.converged = False
        self.lam = lam
        self.do_plot = do_plot

    def estimate(self, x1, x2, init = [], n_dirac=10):
        if self.algorithm == 'top-scipy' or self.algorithm == 'bsn':
            h1, h2 = self._nonneg_anchor_bsn_scipy(x1, x2, init,
                                             self.lam, self.max_iter)

        if self.algorithm == 'top-ista':
            h1, h2 = self._nonneg_anchor_toplitz_ista(x1, x2, init,
                                                       self.lam, self.max_iter)

        if self.algorithm == 'four-ista':
            h1, h2 = self._nonneg_anchor_fourier_ista(x1, x2, init,
                                                   self.lam, self.max_iter)
        
        if self.algorithm == 'crossrel':
            h1, h2 = self._close_form_crossrelation(
                x1, x2, init, self.lam, self.max_iter)
        
        out1 = Channel(
            loc=list(np.arange(len(h1))),
            coeff=list(h1),
            fs=self.fs
        )
        
        out2 = Channel(
            loc=list(np.arange(len(h2))),
            coeff=list(h2),
            fs=self.fs
        )
        
        print(out1)
        print(out2)

        return out1, out2

    def _nonneg_anchor_bsn_scipy(self, x1, x2, init, lam, max_iter):
        '''
        BLIND SPARSE-NONNEGATIVE (BSN) CHANNEL IDENTIFICATION FOR ACOUSTICTIME-DIFFERENCE-OF-ARRIVAL ESTIMATION
        Yuanqing Lin, Jingdong Chen, Youngmoo Kim, and Daniel D. Lee†

        https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.432.2930&rep=rep1&type=pdf

        anchor + non negativity:
        h1*,h2* =       arg  min       1/2 ‖X2h1−X1h2‖2+λ∑j=0[h1(j) +h2(j)]
                            h1,h2
                        subject to  h1(0) = 1,
                                    h1≥0,h2≥0

        Solved with convex nonnegative quadratic programming (NNQP)

        keywords: 
        - Discrete
        - Toeplitz matrices
        - Lasso
        - scipy optimize
        '''

        # Toeplitz matrix transformations
        assert(lam > 0)
        assert(lam <= 1)

        if lam > 1:
            print("Warning: parameter lambda sould be lower than 1")

        x1, x2 = same_lenght_pad_with_zeros(x1,x2)

        N = len(x1)
        my_toepliz = lambda x : conv_toepliz(x,N+self.L-1,self.L)
        X1 = my_toepliz(x1) # (N+L−1)×L
        X2 = my_toepliz(x2) # (N+L−1)×L
        X = np.hstack((X2, -X1))
        L2 = X.shape[1]
        assert X1.shape == (N+self.L-1, self.L)
        assert X2.shape == (N+self.L-1, self.L)
        assert X.shape  == (N+self.L-1, 2*self.L)

        # initialization
        h1_init, h2_init = init['h1'], init['h2']
        h_init = np.concatenate([h1_init, h2_init])
        assert h_init.shape[0] == X.shape[1]

        if self.do_plot:
            plot_lasso_style(np.zeros(N+self.L-1),X,h_init)

        # Lambda max
        yobs = np.copy(X2[:, 0])
        lam_max = np.max(X[:, 1:].transpose() @ yobs)

        lam_pb = lam * lam_max 

        # objective function
        fun = lambda h : 0.5*np.linalg.norm(X@h)**2 + lam_pb * np.sum(h)
        # non negativity constraints
        cstr_nneg = LinearConstraint(np.eye(L2),np.zeros(L2),np.ones(L2))
        # anchor contraint
        cstr_anchor = {"type": "eq",
                       "fun": lambda x: x[0]-1}
        print('Start scypy minimization')
        options = {'disp': True, 
                   'maxiter': 300}
        res = minimize(fun, h_init, constraints=[cstr_anchor, cstr_nneg], options=options, tol=1/self.fs)

        _print_optimization_results(res)
        self.fun = res.fun
        self.it  = res.nit
        self.converged = res.success

        h1, h2 = res.x[:self.L], res.x[self.L:]
        return h1, h2


    def _nonneg_anchor_toplitz_ista(self,x1 ,x2, init, lam, maxit = 10000):
        '''
        BLIND SPARSE-NONNEGATIVE (BSN) CHANNEL IDENTIFICATION FOR ACOUSTICTIME-DIFFERENCE-OF-ARRIVAL ESTIMATION
        Yuanqing Lin, Jingdong Chen, Youngmoo Kim, and Daniel D. Lee†

        https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.432.2930&rep=rep1&type=pdf

        anchor + non negativity:
        h1*,h2* =       arg  min     1/2 ‖X2h1−X1h2‖2+λ∑j=0[h1(j) +h2(j)]
                            h1,h2
                        subject to  h1(0) = 1,
                                    h1≥0,h2≥0

        Solved with an Ista like procedure

        keywords: 
        - Discrete
        - Toeplitz matrices
        - Lasso
        - proximal gradient
        '''

        x1, x2 = same_lenght_pad_with_zeros(x1,x2)

        assert(lam > 0)
        assert(lam <= 1)

        if lam > 1:
            print("Warning: parameter lambda sould be lower than 1")

        # toepliz matrix transformations
        N = len(x1)
        my_toepliz = lambda x : conv_toepliz(x,N+self.L-1,self.L)
        X1 = my_toepliz(x1) # (N+L−1)×L
        X2 = my_toepliz(x2) # (N+L−1)×L

        # Simple trick to return to fitra
        yobs = np.copy(X2[:, 0])
        X2[:, 0] = 0

        X = np.hstack((-X2, + X1))
        L2 = X.shape[1]

        assert X1.shape == (N+self.L-1,self.L)
        assert X2.shape == (N+self.L-1,self.L)
        assert X.shape  == (N+self.L-1,2*self.L)

        # Initilize input
        h1_init, h2_init = init['h1'], init['h2']
        h1_init[0] = 0

        h = np.concatenate([h1_init,h2_init])
        assert h.shape[0] == X.shape[1]

        # Lambda max
        lam_max = np.max(X.transpose() @ yobs)
        lam_pb = lam * lam_max 

        # non negativity constraints
        def pos_soft_thresh(x, lbd):
            return np.maximum(x - lbd, 0.)

        primal = lambda h, Xh : .5 * np.linalg.norm(yobs - Xh, 2)**2 + lam_pb * np.linalg.norm(h, 1)
        dual = lambda u : .5 * (np.linalg.norm(yobs, 2)**2 - np.linalg.norm(yobs - lam_pb * u, 2)**2)

        Xty = X.transpose() @ yobs
        Xh = X @ h
        u = yobs - Xh
        gap = primal(h, Xh) - dual(u)

        tol = 1e-15
        it = 0
        lip = np.linalg.norm(X)**2  # Lipschitz constant
        while gap > tol and it < maxit:
            h_old = np.copy(h)
            h = h + 2 * (Xty - X.transpose() @ Xh) / lip
            h = pos_soft_thresh(h, 2*lam_pb / lip)

            # dual gap
            Xh = X @ h
            u = yobs - Xh
            gap = primal(h, Xh) - dual(u / np.max(X.transpose() @ u))
            it += 1
            not_converged = gap > tol


        print("Ista ended after " + str(it) + " iterations")
        print("dual gap: " + str(gap))
        self.cost = primal(h, Xh)
        self.fun = primal(h, Xh)
        self.it  = it
        self.converged = not not_converged
        self.dual_gap = gap

        # Out
        h1, h2 = h[:self.L], h[self.L:]
        h1[0] = 1
        return h1, h2


    def _nonneg_anchor_fourier_ista(self,x1 ,x2, init, lam, maxit = 10000):
        '''
        BLIND SPARSE-NONNEGATIVE (BSN) CHANNEL IDENTIFICATION FOR ACOUSTICTIME-DIFFERENCE-OF-ARRIVAL ESTIMATION
        Yuanqing Lin, Jingdong Chen, Youngmoo Kim, and Daniel D. Lee†

        https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.432.2930&rep=rep1&type=pdf

        anchor + non negativity:
        h1*,h2* =       arg  min     1/2 ‖X2h1−X1h2‖2+λ∑j=0[h1(j) +h2(j)]
                            h1,h2
                        subject to  h1(0) = 1,
                                    h1≥0,h2≥0

        keywords: 
        - Discrete
        - Fourier matrices
        - Lasso
        - proximal gradient
        '''

        assert(lam > 0)
        assert(lam <= 1)

        if lam > 1:
            print("Warning: parameter lambda sould be lower than 1")

        # make the two array of the same length
        x1, x2 = same_lenght_pad_with_zeros(x1,x2)
        assert x1.size == x2.size
        
        # Fourier matrix transformations
        N = x1.shape[0]
        Fn = DFT_matrix(N) #NxN
        # X1 = (Fn@x1) * Fn # NxN
        # X2 = (Fn@x2) * Fn # NxN

        Fnx1 = Fn@x1
        Fnx2 = Fn@x2
        X1 = np.copy(Fn) # NxN
        X2 = np.copy(Fn) # NxN
        for i in range(N):
            X1[i, :] *= Fnx2[i]
            X2[i, :] *= Fnx1[i]

        assert X1.shape == (N,N)
        assert X2.shape == (N,N)

        # Simple trick to return to fitra
        yobs = np.copy(X1[:, 0]) #Nx1
        #X1[:, 0] = 0

        X = np.hstack((-X1, X2)) #Nx2N
        X = X[:, 1:]
        L2 = X.shape[1]
        assert L2 == 2*N-1

        # Initilize output
        self.L = N
        h1_init, h2_init = init['h1'], init['h2']
        h1_init[0] = 0
        h = np.concatenate([h2_init,h1_init])
        h_init = np.copy(h)

        htilde = h_init[1:]
        if False:
            fig, arrax = plt.subplots(1,3)

            arrax[0].imshow(yobs[:,None])
            arrax[0].set_xticks([])
            arrax[1].imshow(np.vstack([np.real(X),
                                       np.imag(X)]))
            arrax[2].imshow(h[:,None])
            arrax[2].set_xticks([])

            plt.show()

        # Lambda max
        lam_max = np.max(X.real.transpose() @ yobs.real + X.imag.transpose() @ yobs.imag)
        lam_pb = lam * lam_max 

        # Solving
        lasso = Complex_nn_lasso(yobs, X, lam_pb)
        htilde = lasso.run()

        # Printing
        print("four-ista ended after " + str(lasso.nbIt) + " iterations")
        print("dual gap: " + str(lasso.dual_gap))
        self.fun = lasso.dual_gap
        self.it = lasso.nbIt
        self.converged = lasso.converged
        self.dual_gap = lasso.dual_gap

        # Out
        # print(h.shape) # (2t_max)x1
        # print(N)       # (T + t_max - 1)x1 (results of convolution)
        # print(self.L)  # Nx1
        # print(X.shape) # Nx(2N-1)
        # print(yobs.shape) # N
        # print(htilde.shape) # 2N-1 (missing the anchor)
        h1 = np.zeros(self.L)
        h2 = np.zeros(self.L)
        h1[1:] = htilde[:self.L-1]
        h1[0] = 1
        h2[:] = htilde[self.L-1:]
        return h1, h2


    def _close_form_crossrelation(self, x1, x2, init, lam, maxit= 10000):
        x = np.dstack([x1, x2]).squeeze()
        assert x.shape[-1] == 2
        return h1, h2


    # def _mulan(self, x1, x2, init, lam, maxit=10000):
    #     '''
    #     Main algorithm parameters:
    #     m - M microphone measurements in frequency domain(come as a result of chosed filter and input signal)
    #     f - frequency set
    #     K - level of sparsity
    #     alpha - ground truth weights of Diracs
    #     tau - ground truth delays of Diracs
    #     init_option - current one is Random(other are available, but not reliable)
    #     ground_truth - ground truth signal in frequency domain - used for some initialization schemes
    #     '''
    #     # mulan parans
    #     Fs = self.fs
    #     nF = 1000
    #     init_option = InitializationType.Random
    #     K = self.lam
    #     f_range = [200, 8000]

    #     # mulan preprocessing
    #     x = np.dstack([x1, x2]).squeeze()
    #     assert x.shape[-1] == 2
        
    #     X, f, frequency_offset, frequency_step, T_total = \
    #         get_spectral_coefficients_no_time_processing(x, nF, Fs, f_range)

    #     tau, alpha = mulan(X, f, frequency_offset, frequency_step, K, T_total, [], [],
    #                        init_option, [], [], [], max_init_iter=maxit)

    #     out1 = Channel(
    #         toa=[t for t in tau[:, 0]],
    #         coeff=[c for c in alpha[:, 0]],
    #         fs=self.fs
    #     )

    #     out2 = Channel(
    #         toa=[t for t in tau[:, 1]],
    #         coeff=[c for c in alpha[:, 1]],
    #         fs=self.fs
    #     )

    #     return out1, out2


def channel_initial_guess(mode, L, signals=()):
    if mode == 'rnd':
        return {'h1': np.random.random(L),
                'h2': np.random.random(L)}
    if mode == 'gcc':
        lag = lag_finder(signals[0], signals[1], fs=1)
        return {'h1': Channel(loc=[0],   coeff=[1], filter_length=L),
                'h2': Channel(loc=[lag], coeff=[0.8], filter_length=L)}
