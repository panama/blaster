import os
import numpy as np
import pickle as pkl
import json

def save_to_pickle(obj, filename ):
    with open(filename, 'wb') as f:
        pkl.dump(obj, f, pkl.HIGHEST_PROTOCOL)

def load_from_pickle(filename):
    with open(filename, 'rb') as f:
        return pkl.load(f)

def make_dirs(path_to_dir):
    if not os.path.exists(path_to_dir):
        os.makedirs(path_to_dir)

def save_to_json(d, filename):
    json.dump(d, open(filename, 'w'))
