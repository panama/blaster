import numpy as np
from src.dsp_utils import DFT_matrix

for i in range(10,100):
    for j in range(2):
        N = i
        y = np.random.random(N)
        y[0] = 1
        Fn = DFT_matrix(N)

        A = (Fn@y)*Fn
        a = A[:,0]

        assert np.allclose(np.imag(a),np.zeros(N))
        assert np.allclose(a[0]*N,np.sum(y))
