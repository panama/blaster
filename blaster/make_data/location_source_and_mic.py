import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

uninform = lambda dim, a, b : (b-a) * np.random.random(dim) + a

n_sample = 50
room_size = np.array([[6, 4, 3]]).T  # m

src_pos = uninform([3, n_sample], 0.2, 0.8) * \
    np.array([[3, 4, 3]]).T + np.array([[3, 0, 0]]).T
mic1 = uninform([3, n_sample], 0.25, 0.75) * np.array([[3, 4, 3]]).T
mic2 = uninform([3, n_sample], 0.25, 0.75) * np.array([[3, 4, 3]]).T

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.scatter(src_pos[0, :],
        src_pos[1, :],
        src_pos[2, :],
        label='sources')

ax.scatter(mic1[0, :],
        mic1[1, :],
        mic1[2, :],
        label='mic1')

ax.scatter(mic2[0, :],
        mic2[1, :],
        mic2[2, :],
        label='mic2')

for i in range(n_sample):
    ax.plot([mic1[0, i], mic2[0, i]],
            [mic1[1, i], mic2[1, i]],
            [mic1[2, i], mic2[2, i]], alpha=0.1)

for i in range(n_sample):
    ax.plot([(mic1[0, i] + mic2[0, i])/2, src_pos[0, i]],
            [(mic1[1, i] + mic2[1, i])/2, src_pos[1, i]],
            [(mic1[2, i] + mic2[2, i])/2, src_pos[2, i]], alpha=0.1)


plt.legend()
plt.savefig('./data/processed/room_configuration.pdf')
plt.show()


for i in range(n_sample):
    print(np.linalg.norm(mic2[:, i] - mic1[:, i]))

for i in range(n_sample):
    print(src_pos[:, i], mic1[:, i], mic2[:, 2])

np.savetxt('./data/processed/src_pos.csv', src_pos)
np.savetxt('./data/processed/mic1_pos.csv', mic1)
np.savetxt('./data/processed/mic2_pos.csv', mic2)