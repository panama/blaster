import numpy as np


class Complex_nn_lasso():
    """ Complex non-negative Lasso class

        Algorithmic solution to solve

        .. math::

            \\underset{\\mathbf{x}\\in\\mathbb{R}_+^n}{\\arg\\min} 
            \\frac{1}{2}\\Vert \\mathbf{y} - \\mathbf{A}\\mathbf{x}\\Vert_2^2
            + \\lambda \\Vert \\mathbf{x} \\Vert_1

        where :math:`\\mathbf{y}` and :math:`\\mathbf{A}` are complex.
        Note that the solution is real
    """
    def __init__(self, vobs, matA, lam):
        """
        Parameters
        ----------
        - vobs : np.ndarray
            observation vector
            size [m]
        - matA : np.ndarray
            acquisition matrix
            size [m, n]
        - lambda : float
            regularization parameter
        """
        self.vobs = vobs
        self.matA = matA

        self.dual_gap  = np.inf
        self.nbIt = 0
        self.converged = False

        self.lam = lam
        self.sol = np.zeros(matA.shape[1])


    def run(self, maxit=10000, gaptol=1e-15):
        """ Solve the non-negative Lasso problem

        Solve
    
        .. math::

            \\underset{\\mathbf{x}\\in\\mathbb{R}_+^n}{\\arg\\min} 
            \\frac{1}{2}\\Vert \\mathbf{y} - \\mathbf{A}\\mathbf{x}\\Vert_2^2
            + \\lambda \\Vert \\mathbf{x} \\Vert_1

        using a proximal gradient algorithm
        (nonnegative version of Ista)

        Parameters
        ----------
        - maxit : int
            maximum number of iterations
            (default = 10000)
        - gaptol : float
            stopping criteria in terms of duality gap
            (default = 1e-15)
        Returns
        -------
        - xsol : np.ndarray
            solution to the nonnegative complex Lasso problem
            size [n]
        """

        return self._run_impl(self.vobs, self.matA, self.lam, maxit, gaptol)


    def _run_impl(self, yobs, matA, lam, maxit=10000, gap_tol=1e-15):
        """ Internal implementation of the non-negative Lasso problem solver.

        Parameters
        ----------
        - vobs : np.ndarray
            observation vector
            size [m]
        - matA : np.ndarray
            acquisition matrix
            size [m, n]
        - lambda : float
            regularization parameter
        - maxit : int
            maximum number of iterations
            (default = 10000)
        - gaptol : float
            stopping criteria in terms of duality gap
            (default = 1e-15)
        Returns
        -------
        - xsol : np.ndarray
            solution to the nonnegative complex Lasso problem
            size [n]
        """

        soft_thresh = lambda x, lbd: \
            np.maximum(x - lbd, 0.)

        # if not nnegative:
        #     soft_thresh = lambda x, lbd: \
        #         np.sign(x) * np.maximum(np.abs(x) - lbd, 0.)

        # else:

        # 1.a Computing Lipchitz constant
        lip  = np.linalg.norm(matA.conjugate().transpose() @ matA, 2)

        # 1.b Definition primal and dual problems
        primal = lambda xx: \
            + .5 * np.linalg.norm(yobs.real - matA.real @ xx, 2)**2 \
            + .5 * np.linalg.norm(yobs.imag - matA.imag @ xx, 2)**2 \
            + lam * np.linalg.norm(xx, 1)

        dual = lambda ur, ui: \
            + ur.transpose() @ yobs.real \
            + ui.transpose() @ yobs.imag \
            - .5 * np.linalg.norm(ur, 2)**2 \
            - .5 * np.linalg.norm(ui, 2)**2

        # dual = lambda ur, ui: .5 * (\
        #     np.linalg.norm(yobs.real, 2)**2 \
        #     + np.linalg.norm(yobs.imag, 2)**2 \
        #     - np.linalg.norm(yobs.real - lam * ur, 2)**2 \
        #     - np.linalg.norm(yobs.imag - lam * ui, 2)**2 \
        #     )

        # 1.c initialize primal and dual variables
        n = matA.shape[1]
        vx = self.sol
        gap = self.dual_gap

        ureal = yobs.real - matA.real @ vx
        uimag = yobs.imag - matA.imag @ vx
        Atu = matA.real.transpose() @ ureal + matA.imag.transpose() @ uimag
        

        it = 0
        stopping_criterium = lambda g : g <= gap_tol
        converged = stopping_criterium(gap)
        while not converged and it < maxit:
            vx = vx + (1./ lip) * Atu
            vx = soft_thresh(vx, lam / lip)

            ureal = yobs.real - matA.real @ vx
            uimag = yobs.imag - matA.imag @ vx
            Atu = matA.real.transpose() @ ureal + matA.imag.transpose() @ uimag

            gap = primal(vx) - dual(lam * ureal / np.max(Atu), lam * uimag / np.max(Atu))
            # vec_gap[it] = gap

            it += 1
            converged = stopping_criterium(gap)

        self.dual_gap = gap
        self.nbIt += it
        self.sol = np.copy(vx)
        self.converged = converged

        return vx


if __name__ == '__main__':
    n = 1
    matA = np.random.normal(0, 1, (5, n)) + 1j * np.random.normal(0, 1, (5, n))
    x = np.zeros(n)
    # x[2] = 2.
    # x[5] = 1.
    x[0] = 2.
    y = matA @ x
    classo = Complex_nn_lasso(y, matA, 0.005)

    classo.run()
    print("gap: " + str(classo.dual_gap))
    print("nbIt: " + str(classo.nbIt))

