import peakutils
import sys
import numpy as np
import scipy.signal as sg
import matplotlib.pyplot as plt

from mir_eval.onset import validate, f_measure
from mir_eval.util import match_events

def make_same_length(x, y):
    lx = x.shape[0]
    ly = y.shape[0]
    m = ly - lx
    if m == 0:
        return x, y
    elif m > 0:
        z = np.zeros_like(y)
        z[:lx] = x
        x = z
    elif m < 0:
        z = np.zeros_like(x)
        z[:ly] = y
        y = z
    return x, y

def goodnees_of_fit(ch_pred, ch_ref, metrics, appm_thr=0.005):

    nchan = len(ch_pred)

    # here we have np array
    if metrics == 'rmse':
        if len(ch_pred) != len(ch_ref):
            raise ValueError('DimensionMismatchError')

        # pad signal to make them comparable
        for c in range(nchan):
            h_ref = ch_ref[c]
            h_pred = ch_pred[c]
            assert h_ref.shape == h_ref.shape
            ch_pred[c], ch_ref[c] = make_same_length(h_pred, h_ref)
    
    # here we have a list of coefficient to be transformed into numpy array
    if metrics == 'appm':
        for h in [ch_pred, ch_ref]:
            for c in range(nchan):
                for e, loc in enumerate(h[c]):
                    if loc == 0 and e > 0:
                        del h[c][e]

    if metrics == 'xrelation':
        s  = np.random.random(1000)-0.5
        y1 = np.convolve(s, h_ref[0])
        y2 = np.convolve(s, h_ref[1])
        return np.linalg.norm(np.convolve(y1, h_pred[1]) - np.convolve(y2, h_pred[0]))

    # run the metrics
    if metrics == 'rmse':
        return rmse(ch_pred, ch_ref)
    elif metrics == 'appm':
        return average_peak_position_mismatch(ch_pred, ch_ref, appm_thr)
    return

# def rmse(ch_pred, ch_ref):
#     nchan = len(ch_ref)
#     e = np.zeros(nchan)
#     for i in range(len(ch_ref)):
#         err = np.abs(ch_pred[i] - ch_ref[i])
#         e[i] = np.sqrt(np.mean(err**2))
#     return np.mean(e), e


def average_peak_position_mismatch(ch_ref, ch_pred, thr):
    nchan = len(ch_pred)
    f_scores = np.zeros(nchan)
    precision = np.zeros(nchan)
    recall = np.zeros(nchan)
    for c in range(nchan):
        f_scores[c], precision[c], recall[c] = f_measure(
            ch_ref[c], ch_pred[c], window=thr)
    return f_scores, precision, recall


def ch_toa_concat(ch1ch2, offset=10000):
    ch1ch2_toa = np.concatenate([np.array(ch1ch2[0]['toa']),
                                 np.array(ch1ch2[1]['toa']) + offset])
    return ch1ch2_toa


def appm_concat(ch_ref, ch_est, thr):
    ch1ch2_toa_ref = ch_toa_concat(ch_ref)
    ch1ch2_toa_est = ch_toa_concat(ch_est)
    f, p, r = average_peak_position_mismatch(
        [ch1ch2_toa_ref], [ch1ch2_toa_est], thr=thr)
    return f[0], p[0], r[0]


def select_max_n_dirac(ch, D):
    coeff = ch['coeff']
    toa = ch['toa']
    T = len(toa)
    C = len(coeff)
    toa = [toa[i] for i in range(min(T, C))]
    coeff = [coeff[i] for i in range(min(T, C))]
    indeces = np.argsort(coeff)[::-1][:D]
    toa = [toa[i] for i in indeces]
    coeff = [coeff[i] for i in indeces]
    indeces = np.argsort(toa)
    ch['toa'] = [toa[i] for i in indeces]
    ch['coeff'] = [coeff[i] for i in indeces]
    return ch


def select_first_n_dirac(ch, D):
    coeff = ch['coeff']
    toa = ch['toa']
    ch['toa'] = toa[:D]
    ch['coeff'] = coeff[:D]
    return ch

def appm_concat_cond_n_dirac(ch_ref, ch_est, n_dirac, thr):
    M = len(ch_ref)
    for m in range(M):
        ch_est[m] = select_max_n_dirac(ch_est[m], n_dirac)
        ch_ref[m] = select_first_n_dirac(ch_ref[m], n_dirac)
    ch1ch2_toa_ref = ch_toa_concat(ch_ref)
    ch1ch2_toa_est = ch_toa_concat(ch_est)
    f, p, r = average_peak_position_mismatch(
        [ch1ch2_toa_ref], [ch1ch2_toa_est], thr=thr)
    return f[0], p[0], r[0]


def match_peaks(toa_ref, toa_est, thr):
    matched_toa = np.zeros([len(toa_ref), 2])
    c = 0
    if len(toa_est) == 0:
        return np.NaN, 0
    for ref in toa_ref:
        # find the matched picks
        closest = min(toa_est, key=lambda x: abs(x-ref))
        if np.abs(closest - ref) > thr:
            continue
        matched_toa[c, 0] = ref
        matched_toa[c, 1] = closest
        c += 1
    return matched_toa[:c, :], c

def match_peaks_mireval(toa_ref, toa_est, thr):
    matching = match_events(toa_ref, toa_est, thr)
    n_match = len(matching)
    matched_toa = np.zeros([n_match, 2])
    for k in range(n_match):
        (i, j) = matching[k]
        matched_toa[k, 0] = toa_ref[i]
        matched_toa[k, 1] = toa_est[j]
    return matched_toa, n_match
    


def rmse(toa_ref, toa_est, thr):
    # matched_toa, n_match = match_peaks(toa_ref, toa_est, thr) # [toa_ref; toa_est]
    # print(matched_toa)
    matched_toa, n_match = match_peaks_mireval(toa_ref, toa_est, thr)
    if n_match == 0:
        return np.NaN, n_match
    errors = np.abs(matched_toa[:, 0] - matched_toa[:, 1])
    error = np.mean(errors)
    return error, n_match


def rmse_concat(ch_ref, ch_est, thr, fs=None):
    ch1ch2_toa_ref = ch_toa_concat(ch_ref)
    ch1ch2_toa_est = ch_toa_concat(ch_est)
    err, n_match = rmse(ch1ch2_toa_ref, ch1ch2_toa_est, thr)
    if fs is not None:
        err *= fs
    return err, n_match


def rmse_concat_cond_n_dirac(ch_ref, ch_est, n_dirac, thr, fs=None):
    M = len(ch_ref)
    for m in range(M):
        ch_est[m] = select_max_n_dirac(ch_est[m], n_dirac)
        ch_ref[m] = select_first_n_dirac(ch_ref[m], n_dirac)

    ch1ch2_toa_ref = ch_toa_concat(ch_ref)
    ch1ch2_toa_est = ch_toa_concat(ch_est)
    err = rmse(ch1ch2_toa_ref, ch1ch2_toa_est, thr)

    if fs is not None:
        err *= fs
    return err

def findpeaks_n_diracts(ch, n_dirac):
    pass

def prepare_channel_for_evaluation(ch_ref, ch_est, algo, t_max=0.3, thr=0.001, n_dirac=7, Fs=16000, on_grid=False):
    # cut the filter
    M = len(ch_ref)
    for m in range(M):

        toa = np.array(ch_est[m]['toa'])
        coeff = np.array(ch_est[m]['coeff'])
        if not len(toa) == len(coeff):
            coeff = np.array(coeff[:-1])
        # crop the signal to tmax
        idx = np.where(toa <= t_max)
        toa = toa[idx]
        coeff = coeff[idx]
        # peak 7 stronges
        max7_idx = np.argsort(coeff)[::-1][:n_dirac]
        max7_idx = np.sort(max7_idx)
        coeff = coeff[max7_idx]
        toa = toa[max7_idx]

        ch_est[m]['toa'] = toa
        ch_est[m]['coeff'] = coeff

        # assert len(ch_est[m]['toa']) > 0
        # assert len(ch_est[m]['toa']) == len(ch_est[m]['coeff'])

        ##########################
        # the ground truth channels
        toa = np.array(ch_ref[m]['toa'])
        coeff = np.array(ch_ref[m]['coeff'])
        idx = np.where(toa <= t_max)
        toa = toa[idx]
        coeff = coeff[idx]
        max7_idx = np.argsort(coeff)[::-1][:n_dirac]
        max7_idx = np.sort(max7_idx)
        coeff = coeff[max7_idx]
        toa = toa[max7_idx]

        ch_ref[m]['toa'] = toa
        ch_ref[m]['coeff'] = coeff

        assert len(ch_ref[m]['toa']) > 0
        assert len(ch_ref[m]['toa']) == len(ch_ref[m]['coeff'] == n_dirac)

        ch_ref[m]['loc'] = []
        ch_est[m]['loc'] = []

    return ch_ref, ch_est

