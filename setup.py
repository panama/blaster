from setuptools import find_packages, setup

setup(
    name='blaster',
    packages=find_packages(),
    version='0.2.0',
    description='not yet',
    author='Diego Di Carlo and Clement Elvira',
    license='MIT',
)
