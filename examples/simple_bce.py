import numpy as np
import pyroomacoustics as pra
import matplotlib.pyplot as plt

from blaster.blaster import Blaster
from blaster.utils.pra_utils import get_partial_air_and_atf

print('braire here')

Fs = 16000
params = {
    'Fs' : Fs,
    'nfft' : 1024,
}
params['nrfft'] = params['nfft'] + 1
params['L'] = params['nfft']

room_size = [5, 4, 3]
src1_pos = [1, 3, 1.8]
mic1_pos = np.array([4.1, 1.3, 0.33])
mic2_pos = np.array([4.2, 1.1, 0.31])

s1 = np.random.randn(1*Fs)

room = pra.ShoeBox(room_size, fs=16000, max_order=17)

room.add_source(src1_pos, signal=s1)
room.add_microphone_array(
    pra.MicrophoneArray(
        np.concatenate([mic1_pos[:, None], mic2_pos[:, None]], axis=1), Fs))

K0 = 0
K = 2

air, ATF = get_partial_air_and_atf(room, K, params, K0)

h = np.sum(air, -1)
H = np.sum(ATF, -1)

x1 = np.convolve(h[:, 0, 0], s1, 'full')[:Fs]
x2 = np.convolve(h[:, 1, 0], s1, 'full')[:Fs]
t_max = 0.01
max_iter = 100
post_processing = True

# R2 = H[:, 1, 0]/H[:, 0, 0]
# R1 = np.ones_like(R2)

# plt.plot(np.abs(R2))
# plt.show()
# 1/0


algo = Blaster(x1, x2, t_max, Fs, max_iter, input_domain='time', post_processing=post_processing)
algo.run(0.005)
